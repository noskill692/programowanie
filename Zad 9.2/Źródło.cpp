#define _USE_MATH_DEFINES
#include <conio.h>
#include <iostream>
#include <cmath>

using namespace std;

void licz(int p, int q) {
	int wynik = 0;
	if (p % 2 == 0) {
		if (q == 2 || q == 4 || q == 5) {
			wynik = 3 * q + p;
		}
		else {
			wynik = 2 * q;
		}
	}

	else {
		if (q == 1 || q == 3 || q == 6) {
			if (q == p) {
				wynik = 5 * p + 3;
			}
			else {
				wynik = 2 * q + p;
			}
		}
		else {
			if (p < q) {
				wynik = p + 4;
			}
			else {
				wynik = q + 4;
			}
		}
	}

	cout << "Wynik: " << wynik << endl;
}

int main() {
	char znak = 'A';
	int p, q;

	while (znak == 'A') {
		cout << "Wpisz L aby wylosowac liczby, X aby wyjsc: " << endl;
		cin >> znak;
		if (znak == 'X')	return 0;
		p = (double)rand() / (RAND_MAX) * 6 + 1;
		q = (double)rand() / (RAND_MAX) * 6 + 1;
		cout << p << " " << q << endl;
		licz(p, q);
		znak = 'A';
	}
}