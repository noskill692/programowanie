#include <conio.h>
#include <iostream>
#include <cmath>

using namespace std;

int sumaDzielnikow (int liczba) {
	int suma = 0;
	for (int i = 1; i <= sqrt((double)liczba); i++) {
		if(liczba % i == 0)	{
			suma += i;
			if (i > 1) {
				suma += liczba / i;
			}
		}
	}
	return suma;
}

int main() {
	int liczba = 1;
	int suma = 0;

	while (liczba > 0) {
		cout << "Podaj dodatnia liczbe calkowita lub 0 aby wyjsc: ";
		cin >> liczba;
		if (liczba == 0)	break;
		if(sumaDzielnikow(liczba) == liczba)	cout << "Liczba jest liczba doskonala" << endl;
		else	cout << "Liczba nie jest liczba doskonala" << endl;
	}
	return 0;
}