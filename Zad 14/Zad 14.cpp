#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>

using namespace std;

double f(double x) {
	return pow(M_E, -1.5 * x) - (0.3 * pow(x, 2));
}

double licz ( double dokl) {
	double a = 0, b = 1, c;

	if(f(a) == 0) {
		return a;
	}

	if(f(b) == 0) {
		return b;
	}

	do {
        c = (a + b) / 2;

        if (f(c) == 0) {
            return c;
        }

        if (f(a) * f(c) < 0) {
            b = c;
        }

        else {
            a = c;
        }

    } while (abs(a - b) >= dokl);

    return c;
}

int main() {
    double dokl;

	cout << "Podaj dokladnosc: ";
	cin >> dokl;

    cout << licz(dokl);

    return 0;
}
