#include <iostream>

using namespace std;

int silnia(int n) {
	int suma = 1;
	for (int i = 2; i <= n; i++) {
		suma *= i;
	}
	return suma;
}

void oblicz (int n, int k) {
	cout << silnia(n) / (silnia(k) * silnia(n - k));
}

int main() {
	int n, k, m = 0;
	cout << "Podaj k i n: ";
	cin >> k >> n;
	oblicz(n, k);

	cin >> n;
}