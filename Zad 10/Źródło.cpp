#include <conio.h>
#include <iostream>

using namespace std;

void oblicz(int stawka) {
	int godz = 0;
	float suma = 0;
	cout << "Podaj liczbe godzin przepracowanych w tygodniu: ";
	cin >> godz;

	if (godz <= 40) {
		suma = godz * stawka;
	}
	else {
		suma = 40 * stawka;
		suma += (godz - 40) * stawka * 2;
	}
	cout << "Kwota brutto: " << suma << endl;

	if (suma <= 700) {
		suma = suma * 0.85;
	}
	else if (suma > 1200) {
		suma = suma * 0.75;
	}
	else {
		suma = suma * 0.8;
	}

	cout << "Kwota netto: " << suma << endl;
}

int main() {
	char kat = 'Z';
	int stawka = 0;

	while (kat != 'X') {
		switch (kat) {
		case 'A':
			stawka = 15;
			oblicz(stawka);
			kat = 'Z';
			break;
		case 'B':
			stawka = 25;
			oblicz(stawka);
			kat = 'Z';
			break;
		case 'C':
			stawka = 30;
			oblicz(stawka);
			kat = 'Z';
			break;
		case 'D':
			stawka = 35;
			oblicz(stawka);
			kat = 'Z';
			break;
		default:
			cout << "Podaj kategorie (A, B, C lub D) lub X aby wyjsc: ";
			cin >> kat;
			break;
		}
	}
	return 0;
}