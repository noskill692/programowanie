#define _USE_MATH_DEFINES
#include <conio.h>
#include <iostream>
#include <math.h>

using namespace std;

double radiany(double kat) {
	return M_PI * kat / 180;
}

int main() {
	double kat;
	double r;
	double pole;

	cout << "Podaj promien okregu: ";
	cin >> r;

	cout << "Podaj kat w stopniach: ";
	cin >> kat;

	kat = radiany(kat);

	pole = pow(r, 2) / 2 * kat - sin(kat);

	cout << "P = " << pole;

	_getch();
}