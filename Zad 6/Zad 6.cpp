#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	int n;

	cout << "Podaj n > 0: ";
	do {
		cin >> n;
	} while (n <= 0);

	cout << "Dzielniki n:" << endl;

	for (int i = 1; i <= n; i++) {
		if (n % i == 0) {
			cout << i << endl;
		}
	}

	_getch();
}