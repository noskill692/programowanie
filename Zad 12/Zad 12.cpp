#include <iostream>

using namespace std;

void pierwsza(int n) {
	bool pierwsza = true;
	for(int i = 3; i < n; i++) {
		if (n % i == 0) {
			pierwsza = false;
			break;
		}
	}
	if (pierwsza)	cout << n << endl;
}

int main() {
	int n = 1;
	while (n > 0) {
		cout << "Podaj liczbe n >= 2 lub 0 aby wyjsc: ";
		cin >> n;
		if(n == 0)	break;
		cout << 2 << endl;
		for(int i = 3; i <= n; i++) {
			pierwsza(i);
		}
	}
	return 0;
}