#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	int n;
	int wynik = 0;
	cout << "Podaj n > 0: ";
	cin >> n;
	while (n <= 0) {
		cout << "Podaj n > 0: ";
		cin >> n;
	}
	for (int i = 1; i <= n; i++) {
		wynik += i * i;
	}
	for (int i = 1; i <= n; i++) {
		cout << i << " * " << i << " ";
		if (i!=n)	cout << "+ ";
	}
	cout << "= " << wynik << endl;
	_getch();
}