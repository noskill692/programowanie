#include <iostream>
#include <conio.h>

using namespace std;

int main () {

	double a, b, c, d, e, f, w, wx, wy;

	cout << "Podaj liczby \n";
	cin >> a >> b >> c >> d >> e >> f;

	w = a * d - c * b;
	wx = e * d - f * b;
	wy = a * f - c * e;

	if (w != 0) {
		cout.setf(ios::fixed);
		cout.precision(3);
		cout << "x = " << wx/w << " \ny = " << wy/w;
		_getch();
		return 0;
	}

	if (w == 0 && (wx != 0 || wy !=0)) {
		cout << "Uklad jest sprzeczny";
		_getch();
		return 0;
	}

	cout << "Uklad jest nieoznaczony";

	_getch();
	return 0;
}