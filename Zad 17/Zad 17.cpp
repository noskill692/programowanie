#include <iostream>
#include <string>

using namespace std;

bool czyPalindrom (string wyraz) {
    int i = 0;
	int dlugosc = wyraz.length();
	int j = dlugosc - 1;
    int srodek = dlugosc / 2;

	if (dlugosc%2 == 0) {
        while (i < srodek && j >= srodek) {
            if (wyraz[i] != wyraz[j]) {
                return false;
            }
            i++;
            j--;
        }
	}

	else {
        while (i < srodek && j > srodek) {
            if (wyraz[i] != wyraz[j]) {
                return false;
            }
            i++;
            j--;
        }
	}

	return true;
}

int main() {
	string wyraz;

	cout << "Podaj wyraz: ";
	cin >> wyraz;
	if (czyPalindrom(wyraz)) {
        cout << "To jest palindrom";
	}
	else {
        cout << "To nie jest palindrom";
	}
	cin >> wyraz;
	return 0;
}
