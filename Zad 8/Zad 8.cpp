#include <iostream>
#include <conio.h>

using namespace std;

int main () {
	int a, b;
	cout << "Podaj a: ";
	cin >> a;
	cout << "Podaj b: ";
	cin >> b;

	if (a > 0 && b > 0) {
		int a1 = a;
		int b1 = b;
		while (a1 != b1) {
			if (a1 > b1) {
				a1 -= b1;
			}
			else {
				b1 -= a1;
			}
		}
		cout << "NWD = " << a1;
	}
	else {
		cout << "Brak rozwiazania";
	}
	_getch();
}