#define _USE_MATH_DEFINES
#include <conio.h>
#include <iostream>
#include <cmath>

using namespace std;

int main() {
	int p = 0, q = 0, wynik = 0;

	while (p < 1 || p > 6 || q < 1 || q > 6) {
		cout << "Podaj dwie liczby od 1 do 6: " << endl;
		cin >> p;
		cin >> q;
	}

	if (p % 2 == 0) {
		if (q == 2 || q == 4 || q == 5) {
			wynik = 3 * q + p;
		}
		else {
			wynik = 2 * q;
		}
	}

	else {
		if (q == 1 || q == 3 || q == 6) {
			if (q == p) {
				wynik = 5 * p + 3;
			}
			else {
				wynik = 2 * q + p;
			}
		}
		else {
			if (p < q) {
				wynik = p + 4;
			}
			else {
				wynik = q + 4;
			}
		}
	}

	cout << "Wynik: " << wynik << endl;

	system("pause");
	return 0;
}