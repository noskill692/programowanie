#include <iostream>
#include <time.h>

using namespace std;

void pisz_tab(int A[], int n) {
    for (int i = 0; i < n; i++) {
        cout << A[i] << " ";
    }
}

void srednia(int A[], int n) {
    int suma = 0;
    for (int i = 0; i < n; i++) {
        suma += A[i];
    }
    cout << "Srednia: " << suma/n << endl;
}

void ile(int A[], int n) {
    int liczba;
    int ilosc = 0;
    do {
        cout << "Podaj liczbe z przedzialu: ";
        cin >> liczba;
    } while (liczba < 0 || liczba > n);
    for (int i = 0; i < n; i++) {
        if(A[i] == liczba) {
            ilosc++;
        }
    }
    cout << "Ilosc liczb: " << ilosc << endl;
}

void nieparzyste(int A[], int n) {
    int ilosc = 0;
    int suma = 0;

    for(int i = 0; i < n; i++) {
        if(A[i] % 2 == 1) {
            ilosc++;
            suma += A[i];
        }
    }
    cout << ilosc << " liczb nieparzystych." << endl << "Suma: " << suma << endl;
}

void minimum(int A[], int n) {
    int min = A[0];
    int index = 0;
    for (int i = 1; i < n; i++) {
        if (A[i] < min) {
            min = A[i];
            index = i;
        }
    }
    cout << "Najmniejsza liczba: " << min << ". Miejsce: " << index + 1 << endl;
}

void maksimum(int A[], int n) {
    int max = A[0];
    int index = 0;
    for (int i = 1; i < n; i++) {
        if (A[i] > max) {
            max = A[i];
            index = i;
        }
    }
    cout << "Najwieksza liczba: " << max << ". Miejsce: " << index + 1 << endl;
}

void sasiednie(int A[], int n) {
    int a = A[0];
    int b = A[1];
    int suma = a + b;
    for (int i = 2; i < n - 1; i++) {
        if(A[i] + A[i + 1] > suma) {
            a = A[i];
            b = A[i + 1];
            suma = a + b;
        }
    }

    cout << "Sasiednie liczby o najwiekszej sumie: " << a << " + " << b << " = " << suma << endl;
}

void najczestszy(int A[], int n) {
    int tab[100] = {0};
    for (int i = 0; i < n; i++) {
        tab[A[i]]++;
    }
    int max = tab[0];
    for (int i = 0; i < 100; i++) {
        if(tab[i] > max) {
            max = tab[i];
        }
    }
    cout << "Najczesciej wystepujacy element: " << max << endl;
}

int main() {
    srand((unsigned) time(NULL) );
    int a, b, n;
    int tab[100];

    do {
        cout << "Podaj przedzial: ";
        cin >> a >> b;
    } while (a > 100 || b > 100 || a <= 0 || b <= 0);

    do {
        cout << "Podaj ilosc liczb: ";
        cin >> n;
    } while (n < 2 || n > 100);

    for (int i = 0; i < n; i++) {
        tab[i] = (double)rand()/(RAND_MAX)*b+a;
    }

    cout << "Tablica:" << endl;

    pisz_tab(tab, n);

    cout << endl;

    srednia(tab, n);
    ile(tab, n);
    nieparzyste(tab, n);
    minimum(tab, n);
    maksimum(tab, n);
    sasiednie(tab, n);
    najczestszy(tab, n);

    cin >> a;

    return 0;
}
