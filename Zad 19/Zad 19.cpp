#include <iostream>
#include <time.h>
#include <Windows.h>
#include <algorithm>

using namespace std;

int main() {
	srand((unsigned) time(NULL) );

	int t[8][10];

	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 10; j++) {
			t[i][j] = (double)rand ( )/(RAND_MAX)*10+1; 
		}
	}

	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 10; j++) {
			cout << t[i][j] << " "; 
		}
		cout << endl;
	}

	int a = 0, b = 0;

	cout << "Podaj numery wierszy <1, 8>: ";
	while (a < 1 || a > 8)	cin >> a;
	while (b < 1 || b > 8)	cin >> b;

	for (int i = 0; i < 10; i++) {
		swap(t[a-1][i], t[b-1][i]);
	}

	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 10; j++) {
			cout << t[i][j] << " "; 
		}
		cout << endl;
	}


	cin >> t[1][1];
	return 0;
}