#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>

using namespace std;

int menu() {
    cout << "Menu" << endl;
    cout << "1. Pierwiastek funkcji f1(x) = sin(x)cos(x)+5x+1" << endl;
    cout << "2. Pierwiastek funkcji f2(x) = (π-x*x+sin(x))/sqrt(25-x)" << endl;
    cout << "3. Pierwiastek funkcji f3(x) = exp(-1.5x)-0.3x*x" << endl;
    cout << "4. Koniec" << endl;
    int wybor;
    cin >> wybor;
    return wybor;
}

double f1(double x) {
    return sin(x) * cos(x) + 5 * x + 1;
}

double f2(double x) {
    return (M_PI - pow(x, 2) + sin(x)) / sqrt(25 - x);
}

double f3(double x) {
    return pow(M_E, -1.5 * x) - (0.3 * pow(x, 2));
}

double licz(int f, double dokl, double a, double b) {

    double c;

    switch (f) {
        case 1: {
            if(f1(a) == 0) {
                return a;
            }

            if(f1(b) == 0) {
                return b;
            }

            do {
                c = (a + b) / 2;

                if (f1(c) == 0) {
                    return c;
                }

                if (f1(a) * f1(c) < 0) {
                    b = c;
                }

                else {
                    a = c;
                }

            } while (abs(a - b) >= dokl);
            break;
        }

        case 2: {
            if(f2(a) == 0) {
                return a;
            }

            if(f2(b) == 0) {
                return b;
            }

            do {
                c = (a + b) / 2;

                if (f2(c) == 0) {
                    return c;
                }

                if (f2(a) * f2(c) < 0) {
                    b = c;
                }

                else {
                    a = c;
                }

            } while (abs(a - b) >= dokl);
            break;
        }

        case 3: {
            if(f3(a) == 0) {
                return a;
            }

            if(f3(b) == 0) {
                return b;
            }

            do {
                c = (a + b) / 2;

                if (f3(c) == 0) {
                    return c;
                }

                if (f3(a) * f3(c) < 0) {
                    b = c;
                }

                else {
                    a = c;
                }

            } while (abs(a - b) >= dokl);
            break;
        }
    }

    return c;
}

int main() {

    int wybor;

    do {
        wybor = menu();

        double dokl;

        switch (wybor) {
            case 1: {
                cout << "Podaj dokladnosc: ";
                cin >> dokl;
                cout << "Wynik: " << licz(wybor, dokl, -2, 0);
                break;
            }

            case 2: {
                cout << "Podaj dokladnosc: ";
                cin >> dokl;
                cout << "Wynik: " << licz(wybor, dokl, 1.5, 2.2);
                break;
            }

            case 3: {
                cout << "Podaj dokladnosc: ";
                cin >> dokl;
                cout << "Wynik: " << licz(wybor, dokl, 0, 1);
                break;
            }

            case 4: {
                return 0;
            }
        }
    } while (wybor != 4);
}
