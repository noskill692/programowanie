#include <conio.h>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
	int a, b, c;
	do {
		cout << "Podaj trzy dlugosci wieksze od 0: " << endl;
		cin >> a >> b >> c;
	} while (a <= 0 || b <= 0 || c <= 0);

	cout << "Najmniejsza: " << min(min(a, b), c) << endl;

	_getch();
}